import { ModuleDescription, ModuleData, AdventureImage, CaptionImage } from "./IAdventure";
import { ModuleScene } from "./ModuleScene";
import { PdfData, TextDetail } from "../parse";
import * as PFS from "./pfs";

const adventureModule : ModuleDescription = {
    prefix: "pfs-1-05",
    fullName: "PFS #1-05 - Trailblazer's Bounty",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'PATHFINDER SOCIETY SCENARIO #1–05' && t.fontId == 'GoodOT-CondBold:10' && t.page == 1);
    },
    extractDetails: (text: Array<TextDetail>, moduleData: ModuleData) => {
        const i = text.findIndex(x => x.fontId == 'GoodOT-Bold:12' && x.text == 'T');
        if (i !== -1) {
            text.splice(i, 1);
            text[i].text = 'T' + text[i].text;
        }
        return PFS.extractDetails(text, moduleData);
    },
    pages: [
        new PFS.TwoColumnWithRightCutout(3, 500),
        new PFS.TwoColumn(4),
        new PFS.TwoColumn(5),
        new PFS.TwoColumn(6),
        new PFS.TwoColumn(7),
        // 8 is map
        new PFS.TwoColumn(9),
        new PFS.TwoColumn(10),
        // 11 is map
        new PFS.TwoColumn(12),
        // 13 is map
        // 14 is map
        new PFS.TwoColumn(15),
        // 15 is map
        new PFS.TwoColumn(17),
        new PFS.TwoColumn(18),

        // appendix
        new PFS.TwoColumn(19, 'appendix1'),

        new PFS.TwoColumnWithLeftCutout(20, 550, 'appendix2-a1'),
        new PFS.TwoColumnWithRightCutout(21, 550, 'appendix2-a2'),
        new PFS.TwoColumn(22, 'appendix2-b1'),
        new PFS.TwoColumn(23, 'appendix2-b2'),
        new PFS.TwoColumn(24, 'appendix2-c1'),
        new PFS.TwoColumn(25, 'appendix2-c2'),
        new PFS.TwoColumn(26, 'appendix2-d1'),
        new PFS.TwoColumnWithRightCutout(27, 580, 'appendix2-d2'),
        new PFS.TwoColumn(28, 'appendix2-e1'),
        new PFS.TwoColumn(29, 'appendix5-e2'),
    ],

    scenes: [
        new ModuleScene({
            name: 'map1',
            nameInfo: { x: 374.2314, y: 82.9903 },
            page: 8, width: 1313, height: 1051,
            rotateCW: 3,
            imageLeft: 0, imageTop: 0, imageRight: 1313, imageBottom: 1051,
            imageRows: 30, imageCols: 24,
            globalLight: true,
            journals: [
                { x: 750, y: 2550, name: 'A.' }
            ]
        }),
        new ModuleScene({
            name: 'map2',
            nameInfo: { x: 79.6, y: 671.4 },
            nameFn: (name) => name.substring(3).trim(),
            page: 11, width: 975, height: 1300,
            imageLeft: 30, imageTop: 26, imageRight: 963, imageBottom: 1271,
            imageRows: 40, imageCols: 30,
            globalLight: true,
            journals: [
                { x: 1500, y: 2050, name: 'B.' }
            ]
        }),
        new ModuleScene({
            name: 'map3',
            nameInfo: { x: 369.9744, y: 83.2838 },
            page: 13, width: 978, height: 1301,
            imageLeft: 10, imageTop: 13, imageRight: 945, imageBottom: 1288,
            imageRows: 30, imageCols: 22,
            globalLight: true,
            journals: [
                { x: 1050, y: 3250, name: 'C.' }
            ]
        }),
        new ModuleScene({
            name: 'map4',
            nameInfo: { x: 369.4937, y: 82.4518 },
            page: 14, width: 1042, height: 1302,
            rotateCW: 2,
            imageLeft: 11, imageTop: 14, imageRight: 1031, imageBottom: 1289,
            imageRows: 30, imageCols: 24,
            globalLight: true,
            journals: [
                { x: 2400, y: 900, name: 'D.' }
            ],
            walls: [
                {"_id":"VhZG4vHvYPDgUhFu","flags":{},"c":[1712,2525,1712,2725],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"lvRjcES34rxen6Ka","flags":{},"c":[1712,2725,1712,2800],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"oB4Tlj4NIvjp4l2y","flags":{},"c":[1712,2725,1500,2725],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"5lh0cNVbi3mOFJge","flags":{},"c":[1150,2737,987,2737],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"YfUXiH2TdwDkJ9Os","flags":{},"c":[987,2737,987,3000],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"PBcA8SXJcYmyGAG7","flags":{},"c":[1175,3262,987,3287],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"VRH9pKVlD1xx9nVD","flags":{},"c":[987,3287,987,3625],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"sM2J2w6L9nwf7hn8","flags":{},"c":[987,3625,1375,3612],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"jZhN8khoVhZ4ryWX","flags":{},"c":[1375,3612,1375,3412],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"SQ8EMGuQI1FE47Zi","flags":{},"c":[1375,3612,1475,3612],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"npZPKTVXL89VHSnU","flags":{},"c":[1500,3175,1500,3362],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"nMPqiyYg2uiHlgwr","flags":{},"c":[1700,3625,1987,3612],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"NKIVRcO0LuQOP3sA","flags":{},"c":[2287,3600,2287,3712],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"Z0m9eql3On80rEW4","flags":{},"c":[2287,3712,2700,3712],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"B3mC5l7oUhuG4qfM","flags":{},"c":[2700,3712,2700,3212],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"pmfoKkBQlte6Dp4x","flags":{},"c":[2700,3212,2512,3212],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"l0pvKcplItqbNmPX","flags":{},"c":[2287,3600,2225,3612],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"ljUgX1msi5q6M8rJ","flags":{},"c":[2287,3600,2300,3312],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"K0IeLDGcJCuB6Cen","flags":{},"c":[2512,3212,2512,3137],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"FUMFrakg6Ajslkl3","flags":{},"c":[2512,3212,2300,3212],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"kMKTYw3yktUWkvRn","flags":{},"c":[2512,2725,2512,2837],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"IygUtdWpa0PlPXXT","flags":{},"c":[2300,2725,2187,2725],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"olTfMVW7YvObAHD8","flags":{},"c":[2000,2525,2000,2800],"move":1,"sense":1,"door":0,"ds":0}                
            ]
        }),
        new ModuleScene({
            name: 'map5',
            nameInfo: { x: 367.1612, y: 82.4518 },
            page: 16, width: 1300, height: 1045,
            rotateCW: 1,
            imageLeft: 11, imageTop: 14, imageRight: 1031, imageBottom: 1289,
            imageRows: 24, imageCols: 30,
            journals: [
                {"x":2950,"y":2050,"name":"E1."},
                {"x":2650,"y":1350,"name":"E2."},
                {"x":1950,"y":1650,"name":"E3."},
                {"x":1750,"y":2650,"name":"E4."}
            ],
            walls: [
                {"_id":"2BYph5eUEySppL3C","flags":{},"c":[3137,1975,3000,1787],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"fFxuBf538t9hbWEn","flags":{},"c":[3000,1787,3100,1375],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"VZs69mlhZgtyiV5F","flags":{},"c":[3100,1375,3062,1150],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"2g2sCpEMQrtjnS1x","flags":{},"c":[3062,1150,2462,1137],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"WqFhKXJFvRjeY6Rv","flags":{},"c":[2325,1137,2325,1062],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"L0wbt5FTyzJphBQe","flags":{},"c":[2325,1062,2462,1137],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"DhDjgp1b6Hxxa6QO","flags":{},"c":[2325,1137,2462,1137],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"uO5aiObTg4ccRPu7","flags":{},"c":[3062,1150,3062,800],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"3iHLE9y3lq7ibavg","flags":{},"c":[3062,800,2575,825],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"U0tVVfPJLQon78wW","flags":{},"c":[2575,825,2350,800],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"pN3KAWcLkVpN2pCG","flags":{},"c":[2350,800,2187,837],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"JHFzXzBPhowXQkct","flags":{},"c":[2187,837,1975,800],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"74VowsQkzfk8xxXN","flags":{},"c":[1975,800,1725,887],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"ZQkNNXe3PNtReTNp","flags":{},"c":[1725,887,1662,1050],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"x1DRI4J4tt5nMbht","flags":{},"c":[1662,1050,1575,900],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"V6Vx1LzCZsCrKI9p","flags":{},"c":[1575,900,1312,837],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"MLibTtTLPBg7ipYE","flags":{},"c":[1312,837,1112,812],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"XfCf2AaPddHZW9uF","flags":{},"c":[1112,812,862,875],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"8JZLhyP50zPGcXDI","flags":{},"c":[862,875,750,1175],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"FTEOiqqMrxOXu3nZ","flags":{},"c":[750,1175,712,1525],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"RTHKZiqwIMOf2yfh","flags":{},"c":[712,1525,700,1700],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"BYOiXOS65KrCs5hT","flags":{},"c":[700,1700,800,1962],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"L6apArcY7m63JiFm","flags":{},"c":[800,1962,962,1962],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"UMHG2jGHoiDS3OG0","flags":{},"c":[962,1962,1062,1887],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"G0UwQIybdhROYzbe","flags":{},"c":[1062,1887,1200,1862],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"CsEgSSMGwv0CwbeF","flags":{},"c":[962,1962,800,2062],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"az4CT6IsZRIntURy","flags":{},"c":[800,2062,737,2237],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"B3isxnOneMOlvn2I","flags":{},"c":[737,2237,712,2475],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"xA9cde1Oqv7gXRFo","flags":{},"c":[712,2475,700,2862],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"jUZqPJnBECZcy7ks","flags":{},"c":[700,2862,700,3712],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"lua3px5zFqcMOfvt","flags":{},"c":[700,3712,887,3762],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"qZBn19CxOuea02yV","flags":{},"c":[887,3762,3000,3787],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"Rro0wFf8dfIt6YgG","flags":{},"c":[3000,3787,3025,3487],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"tckxKTrkjYD6XRg8","flags":{},"c":[3025,3487,3025,3150],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"IxF7BZf3R8M2KlRD","flags":{},"c":[3025,3150,3012,2687],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"IQmBliWmIY4jCJTS","flags":{},"c":[3012,2687,2862,2375],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"6WEXlzaBYFjnWjwt","flags":{},"c":[2862,2375,2837,2250],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"3TzQujTUvBdhoFVV","flags":{},"c":[2837,2250,3200,2250],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"l6rzr6a3ADJmVe3i","flags":{},"c":[3200,2250,3200,1975],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"rRQN0htje5WryTHN","flags":{},"c":[3200,1975,3137,1975],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"Mwg2OTFHkcT8GmWh","flags":{},"c":[2025,1075,1937,1362],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"VY90aFFYuxdokqt3","flags":{},"c":[1937,1362,1812,1412],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"aidayE9uSGB6lMsD","flags":{},"c":[1812,1412,1612,1362],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"BEUg2knVygjX4jcW","flags":{},"c":[1612,1362,1637,1562],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"vkTXuNZVoanrWYkf","flags":{},"c":[1637,1562,1812,1412],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"Ix8Pcxtq30N5o8R9","flags":{},"c":[1637,1562,1650,1900],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"kQqj39kSgyG4JaFL","flags":{},"c":[1650,1900,1712,1987],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"y1oS3c74Un7DjtXT","flags":{},"c":[1712,1987,1875,2100],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"H7Tiu8nWxErZyRCq","flags":{},"c":[1875,2100,1962,2325],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"JUsvGepIWclQuuBg","flags":{},"c":[1962,2325,1825,2350],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"LI81bVQKaj5Ds2Ae","flags":{},"c":[1825,2350,1875,2100],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"gOnovYYhLwMqc5y5","flags":{},"c":[1825,2350,1675,2437],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"R4EeZZ2OGKRHKJUb","flags":{},"c":[1675,2437,1525,2450],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"6ecYhRElmdaSMIsM","flags":{},"c":[1525,2450,1462,2162],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"pYplT1NUKcRTApt0","flags":{},"c":[1462,2162,1375,2300],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"kOWChhKDjipDwruf","flags":{},"c":[1375,2300,1450,2525],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"1ZJyjEtnWJ13uYnU","flags":{},"c":[1450,2525,1562,2500],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"hI0R5oAPfPELNotL","flags":{},"c":[1562,2500,1675,2437],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"XEqa4FRtqjguNuaK","flags":{},"c":[1212,2850,1175,2762],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"MJp2XxxVdHyEghN3","flags":{},"c":[1175,2762,1037,2875],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"pWZz3fie8czza45s","flags":{},"c":[1037,2875,1000,3000],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"8Pn9xVE3alPxtQIx","flags":{},"c":[1212,2850,1037,2875],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"ErTYmcbJBMcRRgEt","flags":{},"c":[1000,3000,1312,3287],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"lZkdxx2ILA8arC5X","flags":{},"c":[1312,3287,1100,3500],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"kYFaHIsWL0PFnC6A","flags":{},"c":[1312,3287,1675,3300],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"ZdHNvP3EZyaLRiUj","flags":{},"c":[1675,3300,1812,3262],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"ac94YcAz47SsuSVX","flags":{},"c":[1812,3262,1862,3387],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"q2IHd88TRNJxfy4b","flags":{},"c":[1862,3387,1962,3487],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"RBaMQK4l1WQxte15","flags":{},"c":[1962,3487,1887,3525],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"u9wcil1sCF7UxozX","flags":{},"c":[1887,3525,1887,3512],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"3dE7uqNkBdLmVOk6","flags":{},"c":[1887,3512,1737,3400],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"sWlQEG79RwhmjQpU","flags":{},"c":[1737,3400,1675,3300],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"1UtzPzobK3VzTOm7","flags":{},"c":[2325,3512,2562,3387],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"ENTqTKHO2U6R616c","flags":{},"c":[2787,3275,2812,3187],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"9l691nOHeFO4p2TQ","flags":{},"c":[2787,3275,2837,3337],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"BM5agvAws6y2IJy0","flags":{},"c":[2325,3512,2375,3362],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"VrDPrBnutsUf7aNw","flags":{},"c":[2375,3362,2462,3325],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"q6FsMbdPfi0z0dLa","flags":{},"c":[2462,3325,2562,3387],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"VIQiphzP5AI4GOpr","flags":{},"c":[2562,3387,2787,3275],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"FYKjunnmoEYwMcXA","flags":{},"c":[2375,3362,2250,3300],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"cBGDdKvyLyD4dLwj","flags":{},"c":[2250,3300,2175,3162],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"Xo0T97ADqgkQbc8B","flags":{},"c":[2175,3162,2125,2975],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"LCn0HM8a68oYuVx9","flags":{},"c":[2125,2975,2037,2950],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"vfV3BRvsWOqIxCfJ","flags":{},"c":[2037,2950,2175,3162],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"A7kxex1cZJKhzgq9","flags":{},"c":[2162,2550,2200,2762],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"mMm6vmD3IGrwawoz","flags":{},"c":[2200,2762,2425,2800],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"LTzJwtXCMKwqCJeG","flags":{},"c":[2425,2800,2662,2762],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"R7oUYiDorkWSxh4Z","flags":{},"c":[2662,2762,2612,2500],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"GAuqimdZEDrGfQNF","flags":{},"c":[2612,2500,2437,2400],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"fatgWHXnWgSechfB","flags":{},"c":[2437,2400,2250,2537],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"uzdlxQi7aRVOv3S9","flags":{},"c":[2250,2537,2162,2550],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"6SFdIiEn5gDi2q7T","flags":{},"c":[2337,1487,2262,1625],"move":1,"sense":1,"door":0,"ds":0,"locked":false},
                {"_id":"OsfUba4tudfo4dTC","flags":{},"c":[2262,1625,2275,1800],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"1woLsDzddSn1zFVc","flags":{},"c":[2275,1800,2225,1987],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"0swhjYZVfgmQgOpa","flags":{},"c":[2225,1987,2275,2162],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"GiT4TByOf4KffIzx","flags":{},"c":[2275,2162,2612,2037],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"3GsFGoIRgFVdG9aC","flags":{},"c":[2612,2037,2537,1887],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"jOC7GOzzCgkRhmZa","flags":{},"c":[2537,1887,2362,1787],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"WO9yotnc9LGu6x7N","flags":{},"c":[2362,1787,2325,1612],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"o1dDlSutaSrSF06W","flags":{},"c":[2325,1612,2337,1487],"move":1,"sense":1,"door":0,"ds":0}
            ]
        }),
    ],
    journals: [
        new CaptionImage({ name: 'journal1', page: 30, width: 600, height: 600 }),
        new CaptionImage({ name: 'journal2', page: 31, width: 600, height: 600 }),
        new CaptionImage({ name: 'journal3', page: 32, width: 600, height: 600 }),
        new CaptionImage({ name: 'journal4', page: 33, width: 975, height: 1378 }),
        new CaptionImage({ name: 'journal5', page: 34, width: 1066, height: 1449 }),
    ],
    finalize: (moduleData: ModuleData) => {
    }
};

export default adventureModule;